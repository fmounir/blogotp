defmodule BlogOtp.Mixfile do
  use Mix.Project

  def project do
    [app: :blogotp,
     version: "0.0.1",
     elixir: "~> 1.2",
     build_embedded: true,
     start_permanent: Mix.env == :prod,
     aliases: aliases,
     deps: deps]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    [applications: [:logger , :postgrex , :ecto]]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
      [{:cowboy, "~> 1.0.0"},
       {:plug, "~> 1.0"},
       {:postgrex, ">= 0.0.0"},
       {:ecto, ">= 1.1.8"}]
  end

 defp aliases do
    ["ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
     "ecto.reset": ["ecto.drop", "ecto.setup"]]
  end

end
