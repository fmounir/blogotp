defmodule BlogOtp.Plug do
  import Plug.Conn

def init(opts) do 
    opts
end

def sayhello(conn , _opts) do
    IO.inspect("Hello")
    conn
end

## index
def blogapp(%Plug.Conn{request_path: path, method: method } = conn , _opts) when path == "/blogapp" and method == "GET"  do   
       page_content = EEx.eval_file(getTemplatePath("index.eex"), [])
      
       conn 
       |> Plug.Conn.put_resp_content_type("text/html")
       |> Map.put(:resp_body , page_content)
       |> IO.inspect     
end

## edit item by id
def blogapp(%Plug.Conn{request_path: path, method: "GET" , params: %{"id" => idQs}} = conn , _opts) when path == "/blogapp/"<>idQs <> "/edit" do  
      page_content = EEx.eval_file( getTemplatePath("edit.eex"), [])
      
      conn 
       |> Plug.Conn.put_resp_content_type("text/html")      
       |> Map.put(:resp_body , page_content )
       |> IO.inspect
end

## open new item
def blogapp(%Plug.Conn{request_path: path, method: method } = conn , _opts) when path == "/blogapp/new" and method == "GET"  do 
    page_content = EEx.eval_file( getTemplatePath("new.eex"), [])

    conn
    |> Plug.Conn.put_resp_content_type("text/html")    
    |> Map.put(:resp_body , page_content)
    |> IO.inspect
end

## show item by id
def blogapp(%Plug.Conn{request_path: path, method: "GET" , params: %{"id" => idQs}} = conn , _opts) when path == "/blogapp/"<>idQs do   
       page_content = EEx.eval_file( getTemplatePath("show.eex"), [])
      
       conn 
       |> Plug.Conn.put_resp_content_type("text/html")       
       |> Map.put(:resp_body , page_content )
       |> IO.inspect
end

## update action fired for id
def blogapp(%Plug.Conn{request_path: path, method: method , params: %{"id" => idQs}} = conn , _opts) when path == "/blogapp/"<>idQs and method == "PATCH"  do 
       conn 
       |> Map.put(:resp_body , "Update Action fired for : " <> idQs )
       |> IO.inspect
end

## Delete action fired for id
def blogapp(%Plug.Conn{request_path: path, method: method , params: %{"id" => idQs}} = conn , _opts) when path == "/blogapp/"<>idQs and method == "DELETE"  do
       conn 
       |> Map.put(:resp_body , "Delete action fired for id : " <> idQs )
       |> IO.inspect          
end

## create new item
def blogapp(%Plug.Conn{request_path: path, method: method } = conn , _opts) when path == "/blogapp" and method == "POST"  do 
    conn
    |> Plug.Conn.put_resp_content_type("text/html")    
    |> Map.put(:resp_body , "Create new Item")
    |> IO.inspect
end


## any other request that doesnt relate to our app 
def blogapp(%Plug.Conn{request_path: "/" <> name } = conn , _opts) do
    IO.inspect("Not related to blog app")
    conn    
end

defp getTemplatePath(fileName) do
    Path.expand("deps/blogotp/templates/" <> fileName) 
end


end